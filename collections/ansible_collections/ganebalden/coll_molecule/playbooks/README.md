
# Пример playbook:

```
---
- name: Test new role from within this playbook
  hosts: instance 
  become: true  
  gather_facts: true
  tasks:
    - name: Testing role
      no_log: false
      ansible.builtin.include_role:
        name: ganebalden.coll_molecule.my_role
        tasks_from: main.yml
```


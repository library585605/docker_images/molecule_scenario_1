#!/bin/sh
echo "---
- name: Test new role from within this playbook
  hosts: instance 
  become: true  
  gather_facts: true
  tasks:
    - name: Testing role
      no_log: false
      ansible.builtin.include_role:
        name: $name_coll$name_role
        tasks_from: main.yml
" | tee "$path_playbook/my_playbook.yml"
